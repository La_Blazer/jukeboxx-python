import sys, os, re, subprocess, logging
import plsparser
from pprint import pprint
from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.slider import Slider
from kivy.properties import StringProperty, StringProperty, NumericProperty

LAYOUT_PATH = "layout_jukeboxx.kv"
DEBUG = True
LIBRARY_PATH = "C:/Users/La_Blazer/Desktop/Work/Projekty/SOC_radio/library"


# DO NOT CHANGE
song_list = []
stream_list = []
playlist = []

def get_files(path, format):
    files = []
    for filename in os.listdir(path):
        if filename.endswith("." + format):
            files.append(os.path.join(path, filename))
    return files

def reg(regexStr,target):
    mo = re.search(regexStr,target)
    if not mo:
        return "N/A"
    else:
        return mo.group()

def is_number(inputNum):
    try:
        val = int(inputNum)
        if val > 0 and val < 101:
            valid = True
        else:
            valid = False
    except Exception:
        valid = False
    return valid

def get_song_name(path):
    return os.path.splitext(os.path.split(path.replace("\\", "/").decode('unicode_escape').encode('ascii','replace'))[1])[0]

class SongButton(Button):

    filename = StringProperty(None)
    button_type = StringProperty(None) #stream, song or playlist

    def on_press(self):
        #subprocess.call("mpc -q stop", shell=True)
        if self.button_type == "stream" or self.button_type == "playlist":
            subprocess.call("mpc -q insert \"" + self.filename + "\"", shell=True)
            subprocess.call("mpc -q next", shell=True)
            subprocess.call("mpc -q play", shell=True)

        else:
            subprocess.call("mpc -q add \"" + self.filename + "\"", shell=True)
            subprocess.call("mpc -q play", shell=True)

        JukeBoxx.logger.debug("Playing: " + self.filename)


class JukeBoxx(App):
    lbl_playtime = StringProperty()
    lbl_songname = StringProperty()
    num_songperct = NumericProperty(0)
    stream_list = []
    song_list = []

    #set up logging
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    pattern = re.compile(r"^(?=.)(.*\/)?((.*?)((?<=[^\/])\.[^.]+)?)?$") #awesome regular expression, group 0=path, 1=full_filename, 2=filename, 3=extension

    def prev(self):
        subprocess.call("mpc -q prev", shell=True)
        self.logger.debug("Prev pressed")

    def next(self):
        subprocess.call("mpc -q next", shell=True)
        self.logger.debug("Next pressed")

    def mute(self, *args):
        if args[1] == "down":
            subprocess.call("mpc -q volume 0", shell=True)
        else:
            subprocess.call("mpc -q volume 100", shell=True)
        self.logger.debug("Mute pressed " + args[1])

    def play(self):
        subprocess.call("mpc -q toggle", shell=True)
        self.logger.debug("Play/pause pressed")

    def stop(self):
        #subprocess.call("mpc -q stop", shell=True)
        subprocess.call("mpc -q del 0", shell=True)
        self.logger.debug("Stop pressed")

    def slider_touch_up(self, *args):
        if not args[1].grab_state: #if we arent grabbing the slider
            subprocess.call("mpc -q seek " + str(int(self.root.ids.slider.value)) + "%", shell=True)

    def songs(self, state):
        if state == "down":
            self.logger.debug("show_songs")
            self._show_songs()
    
    def streams(self, state):
        if state == "down":
            self.logger.debug("show_streams")
            self._show_streams()

    def playlist(self, state):
        if state == "down":
            self.logger.debug("show_playlist")
            self._show_playlist()

    def _show_songs(self):
        self.root.ids.songlist.clear_widgets()
        
        for path in self.song_list:
            btn = SongButton(text=get_song_name(path), filename=os.path.basename(path), button_type="song") #Remove unicode characters and format from song path and set it as button nam
            self.root.ids.songlist.add_widget(btn)

    def _show_streams(self):
        self.root.ids.songlist.clear_widgets()

        for file in self.stream_list:
            with open(file) as f:
                for entry in plsparser.playlist(f):
                    btn = SongButton(text=entry[1], filename=entry[0], button_type="stream") #Remove unicode characters and format from song path and set it as button nam
                    self.root.ids.songlist.add_widget(btn)
                    self.logger.debug("Streams: " + "Stream: " + (str(entry[0])) + "\tName: " + (str(entry[1])))

    def _show_playlist(self):
        self.root.ids.songlist.clear_widgets()

        for playlist in subprocess.check_output("mpc playlist", shell=True).splitlines():
            btn = SongButton(text=get_song_name(playlist), filename=playlist, button_type="playlist") #Remove unicode characters and format from song path and set it as button nam
            self.root.ids.songlist.add_widget(btn)

    def _scan_libraries(self):
        self.song_list = get_files(LIBRARY_PATH + '/songs', "mp3")
        self.stream_list = get_files(LIBRARY_PATH + '/streams', "pls")

    def _update_info(self, *largs):

        #re.sub(r"\.mp3|m3u|pls|mp4$", "", os.path.basename(subprocess.check_output("mpc current", shell=True)))
        songname = os.path.basename(subprocess.check_output("mpc current", shell=True)).replace(".mp3", "").replace(".mp4", "").replace('\n', ' ').replace('\r', '')

        status = subprocess.check_output("mpc -f \"\"", shell=True)
        playstatus = reg(r"(?<=\n\[)[^}]*(?=\] #)",status)
        playtime = reg(r"\d+:\d{2}\/\d+:\d{2}", status)
        playpercentage = reg(r"(?<= \()[^}]*(?=\%\))", status)
        volume = reg(r"(?<=volume:)[^}]*(?=%)", status)

        if playtime == "N/A":
            self.lbl_playtime = "00:00/00:00"
        else:
            self.lbl_playtime = playtime

        if songname == "":
            self.lbl_songname = "Idle"
        else:
            self.lbl_songname = songname

        if (not playpercentage == "N/A") and is_number(playpercentage):
            self.num_songperct = int(playpercentage)
        else:
            self.num_songperct = 1

        self.logger.debug("Stats: Name: %s Status: %s Time: %s %s Volume: %s" %(songname, playstatus, playtime, playpercentage, volume))

    def _print_fps(self, *largs):
        self.logger.debug('FPS: %d' % Clock.get_rfps()) #priny kivy UI fps
        

    def build(self):
        #load kv layout
        self.root = Builder.load_file(LAYOUT_PATH)

        #scan libraries
        self._scan_libraries()

        #mpd setup
        subprocess.call("mpc crossfade 1", shell=True)
        subprocess.call("mpc consume on", shell=True)
        subprocess.call("mpc clear", shell=True)

        #schedule clock to update mod info every 1 second
        self._update_info()
        Clock.schedule_interval(self._update_info, 1)

        #if debug active print all songs and streams in libraries
        if self.logger.getEffectiveLevel() == logging.DEBUG:
            Clock.schedule_interval(self._print_fps, 2)
            self.logger.debug("Songs found: " + '\n'.join(self.song_list))
            self.logger.debug("Streams found: " + '\n'.join(self.stream_list))

        #display songs in library
        self._show_songs()

        return self.root
            

if __name__ == '__main__':

    if not os.path.isdir(LIBRARY_PATH):
        print('Library %s does not exist' % LIBRARY_PATH)
        sys.exit(1)
    elif not os.path.isdir(LIBRARY_PATH + '\\songs'):
        print('Library %s does not contain songs folder' % LIBRARY_PATH)
        sys.exit(1)
    elif not os.path.isdir(LIBRARY_PATH + '\\streams'):
        print('Library %s does not contain streams folder' % LIBRARY_PATH)
        sys.exit(1)

    JukeBoxx().run()